FROM node:12.16-alpine AS build
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn --production

FROM node:12.16-alpine
WORKDIR /app
COPY index.js ./
COPY --from=build /app/node_modules ./node_modules/

EXPOSE 3000
CMD ["node", "index.js"]
