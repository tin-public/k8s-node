const express = require('express')
const fetch = require('node-fetch')
const mongoose = require('mongoose')
const cors = require('cors')

const port = process.env.NODE_PORT || 3000
const hostname = process.env.HOSTNAME || process.env.HOST || 'NoHostName'
const appname = process.env.NAME || 'NoAppName'
const mongoUri = process.env.MONGO_URI
const apiUrl = process.env.API_URL

const app = express()
app.use(cors())
app.use(express.json())

const serverInfo = {
	isRunnung: false,
	hostname,
	appname,
	port,
	startTime: null,
	mongoState: mongoose.connection.states[99],
}

if (mongoUri) {
	mongoose.connect(mongoUri, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
}

app.get('/', (req, res) => {
	res.json({ ...serverInfo, mongoState: mongoose.connection.states[mongoose.connection.readyState] })
})

app.post('/api-call', async (req, res) => {
	const apiUrl2 = req.body.url || apiUrl
	if (!apiUrl2) {
		res.status(400).json({ message: 'No host connect' })
		return
	}

	try {
		const rawRes = await fetch(apiUrl2)
		const jsonRes = await rawRes.json()

		res.json({
			result: jsonRes,
			url: apiUrl2,
		})
	} catch (error) {
		res.status(500).json({ message: error.message })
	}
})

app.post('/db-connect', async (req, res) => {
	const mongoUri2 = req.body.uri || mongoUri
	if (!mongoUri2) {
		res.status(400).json({ message: 'No mongo uri' })
		return
	}

	try {
		await mongoose.disconnect()

		await mongoose.connect(mongoUri2, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})

		res.json({
			connected: true,
			uri: mongoUri2,
		})
	} catch (error) {
		res.status(400).json({
			connected: false,
			message: error.message,
		})
	}
})

app.post('/db-disconnect', async (req, res) => {
	try {
		await mongoose.disconnect()

		res.json({
			disconnected: true,
		})
	} catch (error) {
		res.status(500).json({
			disconnected: false,
			message: error.message,
		})
	}
})

app.listen(port, () => {
	serverInfo.isRunnung = true
	serverInfo.startTime = new Date().toISOString()
	console.log(`App running\n${JSON.stringify(serverInfo, null, 2)}`)
})
